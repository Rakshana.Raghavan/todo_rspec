class AddCompletedAtToLists < ActiveRecord::Migration[6.1]
  def change
    add_column :lists, :completed_at, :datetime
  end
end
