class AddColumnListstatusToLists < ActiveRecord::Migration[6.1]
  def change
    add_column :lists, :liststatus, :string, null: false, default: "todo"
  end
end
