class AddColumnCompletedToLists < ActiveRecord::Migration[6.1]
  def change
    add_column :lists, :completed, :boolean
  end
end
