class AddColumnCategoryToLists < ActiveRecord::Migration[6.1]
  def change
    add_column :lists, :category, :integer
  end
end

