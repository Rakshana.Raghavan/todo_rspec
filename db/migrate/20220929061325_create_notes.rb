class CreateNotes < ActiveRecord::Migration[6.1]
  def change
    create_table :notes do |t|
      t.string :task
      t.datetime :expiry
      t.integer :status
      t.integer :user_id
      t.integer :category
      t.integer :checkstatus
      t.boolean :completed
      t.integer :liststatus
      t.datetime :completed_at

      t.timestamps
    end
  end
end
