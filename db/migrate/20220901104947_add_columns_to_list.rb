class AddColumnsToList < ActiveRecord::Migration[6.1]
  def change
    add_column :lists, :task, :string
    add_column :lists, :expiry, :datetime
    add_column :lists, :status, :integer
  end
end
