class AddColumnCheckstatusToLists < ActiveRecord::Migration[6.1]
  def change
    add_column :lists, :checkstatus, :integer, :default => 0
  end
end
