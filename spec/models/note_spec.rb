require 'rails_helper'

RSpec.describe Note, type: :model do
	describe "Add Todo List Item to Todo List" do
		context "Todo List Items Associations" do
	  		it { is_expected.to belong_to :user }
		end

		context "Todo List Items Validations" do
			it { is_expected.to validate_presence_of :status }
		end 

        
	end
end