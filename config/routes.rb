Rails.application.routes.draw do
  resources :users
  resources :lists do
     member do
      patch :complete
     end
    end
  root 'pages#index'
  get 'log-in', to: 'sessions#new'
  post 'log-in', to: 'sessions#create'
  delete 'log-out', to: 'sessions#destroy'
  get 'log-out', to: 'sessions#destroy'
  post '/lists/new', to: 'lists#new'
  get '/yesterday', to: 'lists#yesterday'
  get '/today', to: 'lists#today'
  get '/personal', to: 'lists#personal'
  get '/grocery', to: 'lists#grocery'
  get '/todo', to: 'lists#todo'
  get '/work', to: 'lists#work'
  get '/completed', to: 'lists#completed'
  namespace :api do
    namespace :v1 do
      resources :notes
    end
  end

  # get 'pages/index'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
