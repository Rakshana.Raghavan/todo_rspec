import { Controller } from "stimulus"
export default class extends Controller {
    static targets = [ "completed" ]
    toggle(event) {
        let formData = new FormData()
        formData.append("list[completed]", this.completedTarget.checked);
    }
}