module Api
    module V1
     class NotesController < ApplicationController

     before_action :set_list, only: %i[ show ]

  def index
    @lists = Note.all
    render json:{status: true, tasks: @lists.as_json}
  end

  def show
    @lists=User.all
    render json:{status: true, tasks: @lists.as_json}

  end


  def new
    @list = Note.new
  end


  def edit
  end

def update
  @list = Note.find_by_id params[:id]
  @list.update list_params
  @list.save
  # render json: @list
  render json:{status: true, tasks: @list.as_json}
end

  
  def create 
    @list = Note.new(list_params)
    @list.user_id = 1
      if @list.save
        render json: @list
      else
        render json:{status: false}
        # render json: @list
      end
    end


  def complete
    @list = Note.find_by_id params[:id]
    @list.update_column(:checkstatus,1)
    if @list.save
     redirect_to '/lists', notice: "Todo item completed"
    end
    render json:{status: true, tasks: @list.as_json}
   end

  def destroy
    @list = Note.find(params[:id])

    if @list.destroy
        render json:{status: true, tasks: @list.as_json}
    else
        flash[:notice] = "Reminder type deletion failed"
    end
end
  
  def yesterday
    lists_users = Note.where(user_id: current_user.id)
    @lists=lists_users.where(created_at: Date.today..Date.today+7).paginate(page: params[:page], per_page: 10)
    # render json: @lists
  end
  def today
    lists_user = Note.where(user_id: current_user.id)
    @lists=lists_user.where("DATE(expiry) = ?", Date.today).paginate(page: params[:page], per_page: 10)
    # render json: @lists
  end
  def personal
    @lists=Note.where(user_id: current_user.id,category: 1)
    # render json: @lists
  end
  def work
    @lists=Note.where(user_id: current_user.id,category: 2)
    # render json: @lists
  end
  def grocery
    @lists=Note.where(user_id: current_user.id,category: 0)
    # render json: @lists
  end
  def todo
    @lists=Note.where(user_id: current_user.id,checkstatus: 0)
    # render json: @lists
  end
  def completed
    @lists=Note.where(user_id: current_user.id,checkstatus: 1)
    # render json: @lists
  end
  

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_list
      @list = Note.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def list_params
      params.permit(:task, :expiry, :status, :category, :completed, :user_id )
    end

end
end 
end
