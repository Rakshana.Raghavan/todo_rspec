class ListsController < ApplicationController
    before_action :set_list, only: %i[ show ]

  def index
    @lists = List.where(user_id: current_user.id).paginate(page: params[:page], per_page: 10)
  end

  def show
    @lists=User.find_by_id params[:id]

    # render json: @lists
  end


  def new
    @list = List.new
  end


  def edit
  end

def update
  @list = List.find_by_id params[:id]
  @list.update list_params
  @list.save
  # render json: @list
  respond_to do |format|
    format.js #add this at the beginning to make sure the form is populated.
  end
end

  
  def create
    @list = List.new(list_params)
    @list.user = current_user
    respond_to do |format|
      if @list.save
        format.html { redirect_to '/lists', notice: "Task was successfully created." }
        # render json: @list
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @list.errors, status: :unprocessable_entity }
      end
    end
  end

  def complete
    @list = List.find_by_id params[:id]
    @list.update_column(:checkstatus,1)
    if @list.save
     redirect_to '/lists', notice: "Todo item completed"
    end
   end

  def destroy
    @list = List.find(params[:id])

    if @list.destroy
        flash[:notice] = "Reminder type has been deleted successfully"
        redirect_to lists_path
    else
        flash[:notice] = "Reminder type deletion failed"
    end
end
  
  def yesterday
    lists_users = List.where(user_id: current_user.id)
    @lists=lists_users.where(created_at: Date.today..Date.today+7).paginate(page: params[:page], per_page: 10)
    # render json: @lists
  end
  def today
    lists_user = List.where(user_id: current_user.id)
    @lists=lists_user.where("DATE(expiry) = ?", Date.today).paginate(page: params[:page], per_page: 10)
    # render json: @lists
  end
  def personal
    @lists=List.where(user_id: current_user.id,category: 1)
    # render json: @lists
  end
  def work
    @lists=List.where(user_id: current_user.id,category: 2)
    # render json: @lists
  end
  def grocery
    @lists=List.where(user_id: current_user.id,category: 0)
    # render json: @lists
  end
  def todo
    @lists=List.where(user_id: current_user.id,checkstatus: 0)
    # render json: @lists
  end
  def completed
    @lists=List.where(user_id: current_user.id,checkstatus: 1)
    # render json: @lists
  end
  

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_list
      @list = List.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def list_params
      params.require(:list).permit(:task, :expiry, :status, :category, :completed)
    end

end
