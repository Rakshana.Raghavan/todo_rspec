class ApplicationController < ActionController::Base
    skip_before_action :verify_authenticity_token
    before_action :current_user
    helper_method :current_user, :logged_in?
    add_flash_types :info, :error, :warning
      def current_user
        @_current_user ||= session[:current_user_id] &&
        User.find_by(id: session[:current_user_id])
    end
    def logged_in?
        !!current_user 
    end 
    def require_user 
        if !logged_in?
            flash[:alert] = "You must be logged in to continue."
            redirect_to login_path
        end
    end
end
