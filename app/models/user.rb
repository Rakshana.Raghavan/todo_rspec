class User < ApplicationRecord
    has_secure_password
    validates :name, presence: true, uniqueness: true, length: { minimum: 3 }
    validates :email, presence: true, uniqueness: true
    validates :password, presence: true, length: { minimum: 6 }
    
    has_many :lists
    has_many :notes
end
