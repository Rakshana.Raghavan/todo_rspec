class Note < ApplicationRecord
    belongs_to :user
    validates :status, presence: true
    enum checkstatus: [ :ToDo, :Completed]
    enum category: [:GroceryList, :Personal, :Work]
end