class List < ApplicationRecord
    belongs_to :user
    validates :status, presence: true
    enum checkstatus: [ :ToDo, :Completed]
    enum category: [:GroceryList, :Personal, :Work]
    def completed?
        !completed_at.blank?
    end
end
